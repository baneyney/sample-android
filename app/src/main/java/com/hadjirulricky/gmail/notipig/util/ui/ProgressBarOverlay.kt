package com.hadjirulricky.gmail.notipig.util.ui

import androidx.lifecycle.MutableLiveData

class ProgressBarOverlay {
    val show = MutableLiveData(false)

    fun show() {
        show.postValue(true)
    }

    fun hide() {
        show.postValue(false)
    }
}