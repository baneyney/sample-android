package com.hadjirulricky.gmail.notipig.util

object Constants {

    object Value {
        const val LAUNCH_TIMER = 3000L
    }

    object SharedPreferencesKey {
        const val SHARED_PREF_NAME = "SWINE_TASK_MANAGER"
        const val LOGIN_COMPLETED = "LOGIN_COMPLETED"
        const val FIRST_INSTALL = "FIRST_INSTALL"
    }
}