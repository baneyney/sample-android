package com.hadjirulricky.gmail.notipig.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.ActivityMainBinding
import com.hadjirulricky.gmail.notipig.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_onboarding.nav_host_fragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        val navHostFragment = nav_host_fragment as NavHostFragment
        val navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.todayTaskFragment,
                R.id.missedTaskFragment,
                R.id.allTaskFragment,
                R.id.manageTasksFragment,
                R.id.settingsFragment
            ),
            binding.drawerLayout
        )
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
        binding.navView.setCheckedItem(R.id.todayTaskFragment)
    }
}