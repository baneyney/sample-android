package com.hadjirulricky.gmail.notipig.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hadjirulricky.gmail.notipig.di.ViewModelFactory
import com.hadjirulricky.gmail.notipig.di.ViewModelKey
import com.hadjirulricky.gmail.notipig.ui.launch.LaunchActivityViewModel
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivityViewModel
import com.hadjirulricky.gmail.notipig.ui.onboarding.signin.SignInFragmentViewModel
import com.hadjirulricky.gmail.notipig.ui.onboarding.signup.SignUpFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelBindingModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LaunchActivityViewModel::class)
    abstract fun bindLaunchActivityViewModel(
        viewModel: LaunchActivityViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnBoardingActivityViewModel::class)
    abstract fun bindOnboardingActivityViewModel(
        viewModel: OnBoardingActivityViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignUpFragmentViewModel::class)
    abstract fun bindSignUpFragmentViewModel(
        viewModel: SignUpFragmentViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignInFragmentViewModel::class)
    abstract fun bindSignInFragmentViewModel(
        viewModel: SignInFragmentViewModel
    ): ViewModel
}