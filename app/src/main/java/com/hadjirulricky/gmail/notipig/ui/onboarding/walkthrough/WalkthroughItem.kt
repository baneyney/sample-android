package com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough

data class WalkthroughItem(val image: Int, val message: String)