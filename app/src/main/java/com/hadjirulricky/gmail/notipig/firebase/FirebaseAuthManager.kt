package com.hadjirulricky.gmail.notipig.firebase

import com.google.firebase.auth.FirebaseAuth
import com.hadjirulricky.gmail.notipig.util.Resource
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FirebaseAuthManager {
    private val firebaseAuth = FirebaseAuth.getInstance()

    suspend fun createUserWithEmailAndPassword(
        email: String,
        password: String
    ): Resource<String> {
        val result = CompletableDeferred<Resource<String>>()
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                result.complete(Resource.Success(it.user.uid))
            }
            .addOnFailureListener {
                result.complete(Resource.Error(it.message ?: ""))
            }
        return result.await()
    }

    suspend fun signInWithEmailAndPassword(
        email: String,
        password: String
    ): Resource<String> {
        val result = CompletableDeferred<Resource<String>>()
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                result.complete(Resource.Success(it.user.uid))
            }
            .addOnFailureListener {
                result.complete(Resource.Error(it.message ?: ""))
            }
        return result.await()
    }
}