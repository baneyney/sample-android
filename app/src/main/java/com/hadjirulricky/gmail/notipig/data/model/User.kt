package com.hadjirulricky.gmail.notipig.data.model

import com.hadjirulricky.gmail.notipig.annotation.TestOpen

@TestOpen
data class User(
    val uid: String = "",
    val email: String = "",
    val firstName: String = "",
    val lastName: String = ""
)