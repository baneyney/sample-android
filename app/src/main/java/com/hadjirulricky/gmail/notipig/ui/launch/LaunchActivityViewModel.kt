package com.hadjirulricky.gmail.notipig.ui.launch

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadjirulricky.gmail.notipig.util.Constants.SharedPreferencesKey.LOGIN_COMPLETED
import com.hadjirulricky.gmail.notipig.util.Constants.Value.LAUNCH_TIMER
import com.hadjirulricky.gmail.notipig.util.livedata.Event
import com.hadjirulricky.gmail.notipig.util.manager.ISharedPreferencesManager
import javax.inject.Inject

class LaunchActivityViewModel @Inject constructor(
    private val sharedPreferencesManager: ISharedPreferencesManager
) : ViewModel() {
    val launchDestination = MutableLiveData<Event<LaunchDestination>>()

    fun startLaunchCountdown() {
        val runnable = Runnable { onLaunchScreenCountDownEnded() }
        Handler().postDelayed(runnable, LAUNCH_TIMER)
    }

    fun onLaunchScreenCountDownEnded() {
        val loginCompleted = sharedPreferencesManager.getBoolean(LOGIN_COMPLETED)
        launchDestination.value = if (loginCompleted) {
            Event(LaunchDestination.MAIN_ACTIVITY)
        } else {
            Event(LaunchDestination.ONBOARDING)
        }
    }
}

enum class LaunchDestination {
    ONBOARDING,
    MAIN_ACTIVITY
}