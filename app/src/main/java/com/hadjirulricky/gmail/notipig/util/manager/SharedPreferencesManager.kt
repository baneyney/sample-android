package com.hadjirulricky.gmail.notipig.util.manager

import android.content.Context
import android.content.SharedPreferences
import com.hadjirulricky.gmail.notipig.util.Constants

class SharedPreferencesManager(context: Context) : ISharedPreferencesManager {

    companion object {
        private const val SHARED_PREFERENCE_NAME = Constants.SharedPreferencesKey.SHARED_PREF_NAME
    }

    private var sharedPreferences: SharedPreferences
    init {
        sharedPreferences = context.getSharedPreferences(
            SHARED_PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )
    }

    override fun getString(
        key: String,
        defaultValue: String
    ) = sharedPreferences.getString(key, defaultValue)

    override fun getInt(
        key: String,
        defaultValue: Int
    ) = sharedPreferences.getInt(key, defaultValue)

    override fun getLong(
        key: String,
        defaultValue: Long
    ) = sharedPreferences.getLong(key, defaultValue)

    override fun getBoolean(
        key: String,
        defaultValue: Boolean
    ) = sharedPreferences.getBoolean(key, defaultValue)

    override fun <T> put(key: String, value: T) {
        sharedPreferences.edit().apply {
            when (value) {
                is Int -> putInt(key, value)
                is Boolean -> putBoolean(key, value)
                is Float -> putFloat(key, value)
                is Long -> putLong(key, value)
                is String -> putString(key, value)
            }
        }.apply()
    }
}

interface ISharedPreferencesManager {
    fun getString(key: String, defaultValue: String = ""): String
    fun getInt(key: String, defaultValue: Int = -1): Int
    fun getLong(key: String, defaultValue: Long = -1L): Long
    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean
    fun <T> put(key: String, value: T)
}
