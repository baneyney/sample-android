package com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadjirulricky.gmail.notipig.util.livedata.Event

class WalkthroughFragmentViewModel : ViewModel() {
    val navigateToNextScreen = MutableLiveData<Event<Boolean>>()

    fun onStartButtonClicked() {
        navigateToNextScreen.value = Event(true)
    }
}