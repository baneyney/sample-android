package com.hadjirulricky.gmail.notipig.ui.onboarding.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.data.repository.IUserRepository
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.util.FieldValidator
import com.hadjirulricky.gmail.notipig.util.livedata.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignUpFragmentViewModel @Inject constructor(
    private val userRepository: IUserRepository
) : ViewModel() {
    private val fieldValidator = FieldValidator()
    private val _signUpNetworkResponse = MutableLiveData<Event<Resource<User>>>()
    val signUpNetworkResponse: LiveData<Event<Resource<User>>> = _signUpNetworkResponse

    private val _showTermsAndConditions = MutableLiveData<Event<Boolean>>()
    val showTermsAndConditions: LiveData<Event<Boolean>> = _showTermsAndConditions

    private val _navigateBack = MutableLiveData<Event<Boolean>>()
    val navigateBack: LiveData<Event<Boolean>> = _navigateBack

    val firstName = MutableLiveData("")
    val lastName = MutableLiveData("")
    val email = MutableLiveData("")
    val password = MutableLiveData("")
    val showErrors = MutableLiveData(false)

    fun onCreateAccountButtonClicked() {
        if (validInputs()) {
            showErrors.value = false
            signUp()
        } else {
            showErrors.value = true
        }
    }

    fun onTermsAndConditionsClicked() {
        _showTermsAndConditions.value = Event(true)
    }

    fun onBackButtonClicked() {
        _navigateBack.value = Event(true)
    }

    private fun signUp() {
        _signUpNetworkResponse.postValue(Event(Resource.Loading()))
        viewModelScope.launch {
            val result = userRepository.signUp(
                email.value ?: "",
                password.value ?: "",
                firstName.value ?: "",
                lastName.value ?: ""
            )
            _signUpNetworkResponse.postValue(Event(result))
        }
    }

    private fun validInputs(): Boolean {
        return fieldValidator.notEmpty(firstName.value ?: "") &&
            fieldValidator.notEmpty(lastName.value ?: "") &&
            fieldValidator.validEmail(email.value ?: "") &&
            fieldValidator.validPassword(password.value ?: "")
    }
}