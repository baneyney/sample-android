package com.hadjirulricky.gmail.notipig.firebase

import com.google.firebase.firestore.FirebaseFirestore
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.util.Resource
import kotlinx.coroutines.CompletableDeferred

class FirebaseManager {
    private val firebaseAuthManager = FirebaseAuthManager()
    private val firebaseFirestore = FirebaseFirestore.getInstance()

    suspend fun signUp(
        email: String,
        password: String,
        firstName: String,
        lastName: String
    ): Resource<User> {
        val result = CompletableDeferred<Resource<User>>()
        val authenticationResult = firebaseAuthManager.createUserWithEmailAndPassword(
            email,
            password
        )
        when (authenticationResult) {
            is Resource.Success -> {
                val uid = authenticationResult.data ?: ""
                val user = User(
                    uid,
                    email,
                    firstName,
                    lastName
                )
                val insertUserResult = insertUser(user)
                result.complete(insertUserResult)
            }
            is Resource.Error -> {
                result.complete(Resource.Error(authenticationResult.message ?: ""))
            }
            else -> {}
        }
        return result.await()
    }

    suspend fun signIn(email: String, password: String): Resource<User> {
        val result = CompletableDeferred<Resource<User>>()
        val signInResult = firebaseAuthManager.signInWithEmailAndPassword(
            email,
            password
        )
        when (signInResult) {
            is Resource.Success -> {
                val uid = signInResult.data ?: ""
                val getUserInfoResult = getUserInfo(uid)
                result.complete(getUserInfoResult)
            }
            is Resource.Error -> {
                result.complete(Resource.Error(signInResult.message ?: ""))
            }
        }
        return result.await()
    }

    private suspend fun getUserInfo(uid: String): Resource<User> {
        val result = CompletableDeferred<Resource<User>>()
        firebaseFirestore.collection("users")
            .document(uid)
            .get()
            .addOnSuccessListener {
                var user = it.toObject(User::class.java)
                if (user == null) {
                    user = User(uid = uid)
                    result.complete(Resource.Success(user))
                } else {
                    result.complete(Resource.Success(user))
                }
            }
            .addOnFailureListener {
                result.complete(Resource.Error(it.message ?: ""))
            }
        return result.await()
    }

    private suspend fun insertUser(user: User): Resource<User> {
        val result = CompletableDeferred<Resource<User>>()
        firebaseFirestore.collection("users")
            .document(user.uid)
            .set(user)
            .addOnCompleteListener {
                result.complete(Resource.Success(user))
            }
            .addOnFailureListener {
                result.complete(Resource.Error(it.message ?: ""))
            }
        return result.await()
    }
}