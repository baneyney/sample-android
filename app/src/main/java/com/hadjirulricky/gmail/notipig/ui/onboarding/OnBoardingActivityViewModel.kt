package com.hadjirulricky.gmail.notipig.ui.onboarding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.util.Constants
import com.hadjirulricky.gmail.notipig.util.livedata.Event
import com.hadjirulricky.gmail.notipig.util.manager.ISharedPreferencesManager
import javax.inject.Inject

class OnBoardingActivityViewModel @Inject constructor(
    private val sharedPreferencesManager: ISharedPreferencesManager
) : ViewModel() {
    val startDestination = MutableLiveData<Event<Int>>()

    fun determineStartDestination() {
        val firstInstall = sharedPreferencesManager.getBoolean(
            Constants.SharedPreferencesKey.FIRST_INSTALL,
            true
        )
        if (!firstInstall) {
            startDestination.value = Event(R.id.signInFragment)
        } else {
            sharedPreferencesManager.put(Constants.SharedPreferencesKey.FIRST_INSTALL, false)
            startDestination.value = Event(R.id.walkthroughFragment)
        }
    }
}