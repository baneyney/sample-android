package com.hadjirulricky.gmail.notipig.ui.onboarding.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.data.repository.IUserRepository
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.util.FieldValidator
import com.hadjirulricky.gmail.notipig.util.livedata.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignInFragmentViewModel @Inject constructor(
    private val userRepository: IUserRepository
) : ViewModel() {
    val email = MutableLiveData("")
    val password = MutableLiveData("")
    val showErrors = MutableLiveData(false)

    private val _navigateToSignUp = MutableLiveData<Event<Boolean>>()
    val navigateToSignUp: LiveData<Event<Boolean>> = _navigateToSignUp

    private val _signInNetworkResponse = MutableLiveData<Event<Resource<User>>>()
    val signInNetworkResponse: LiveData<Event<Resource<User>>> = _signInNetworkResponse

    fun onCreateAccountClicked() {
        _navigateToSignUp.value = Event(true)
    }

    fun onSignInButtonClicked() {
        val fieldValidator = FieldValidator()
        val validInputs = fieldValidator.validEmail(email.value ?: "") &&
            (password.value ?: "").isNotEmpty()
        if (validInputs) {
            signIn()
        } else {
            showErrors.value = true
        }
    }

    private fun signIn() {
        _signInNetworkResponse.postValue(Event(Resource.Loading()))
        viewModelScope.launch {
            val result = userRepository.signIn(
                email.value ?: "",
                password.value ?: ""
            )
            _signInNetworkResponse.postValue(Event(result))
        }
    }
}