package com.hadjirulricky.gmail.notipig.util.extension

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

fun Fragment.findNavController(): NavController {
    return NavHostFragment.findNavController(this)
}