package com.hadjirulricky.gmail.notipig.ui.onboarding.signin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.FragmentSignInBinding
import com.hadjirulricky.gmail.notipig.di.Injectable
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.ui.main.MainActivity
import com.hadjirulricky.gmail.notipig.util.FieldValidator
import com.hadjirulricky.gmail.notipig.util.extension.findNavController
import com.hadjirulricky.gmail.notipig.util.extension.hideSoftKeyboard
import com.hadjirulricky.gmail.notipig.util.extension.showAlertDialog
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver
import com.hadjirulricky.gmail.notipig.util.ui.ProgressBarOverlay
import javax.inject.Inject

class SignInFragment : Fragment(), Injectable {
    private lateinit var binding: FragmentSignInBinding
    private lateinit var viewModel: SignInFragmentViewModel
    private lateinit var progressBarOverlay: ProgressBarOverlay

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_sign_in,
            null,
            false
        )

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(SignInFragmentViewModel::class.java)

        progressBarOverlay = ProgressBarOverlay()

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.fieldValidator = FieldValidator()
        binding.progressBarOverlay = progressBarOverlay

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.navigateToSignUp.observe(
            viewLifecycleOwner,
            EventObserver { navigateToSignUp ->
                if (!navigateToSignUp) return@EventObserver
                findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
            }
        )

        viewModel.signInNetworkResponse.observe(
            viewLifecycleOwner,
            EventObserver { response ->
                when (response) {
                    is Resource.Loading -> {
                        progressBarOverlay.show()
                        context?.hideSoftKeyboard(binding.root)
                    }
                    is Resource.Success -> {
                        progressBarOverlay.hide()
                        startActivity(Intent(context, MainActivity::class.java))
                        activity?.finish()
                    }
                    is Resource.Error -> {
                        progressBarOverlay.hide()
                        context.showAlertDialog(
                            getString(R.string.error_sign_in_failed),
                            response.message,
                            getString(R.string.ok)
                        )
                    }
                }
            }
        )
    }
}