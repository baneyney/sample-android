package com.hadjirulricky.gmail.notipig.ui.onboarding.signup

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.FragmentSignUpBinding
import com.hadjirulricky.gmail.notipig.di.Injectable
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.ui.main.MainActivity
import com.hadjirulricky.gmail.notipig.util.FieldValidator
import com.hadjirulricky.gmail.notipig.util.extension.findNavController
import com.hadjirulricky.gmail.notipig.util.extension.hideSoftKeyboard
import com.hadjirulricky.gmail.notipig.util.extension.showAlertDialog
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver
import com.hadjirulricky.gmail.notipig.util.ui.ProgressBarOverlay
import javax.inject.Inject

class SignUpFragment : Fragment(), Injectable {
    private lateinit var binding: FragmentSignUpBinding
    private lateinit var viewModel: SignUpFragmentViewModel
    private lateinit var progressBarOverlay: ProgressBarOverlay

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_sign_up,
            null,
            false
        )

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(SignUpFragmentViewModel::class.java)

        progressBarOverlay = ProgressBarOverlay()

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.fieldValidator = FieldValidator()
        binding.progressBarOverlay = progressBarOverlay

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.signUpNetworkResponse.observe(viewLifecycleOwner, EventObserver { response ->
            when (response) {
                is Resource.Loading -> {
                    progressBarOverlay.show()
                    context?.hideSoftKeyboard(binding.root)
                }
                is Resource.Success -> {
                    progressBarOverlay.hide()
                    startActivity(Intent(context, MainActivity::class.java))
                    activity?.finish()
                }
                is Resource.Error -> {
                    progressBarOverlay.hide()
                    context.showAlertDialog(
                        getString(R.string.error_sign_up_failed),
                        response.message,
                        getString(R.string.ok)
                    )
                }
            }
        })

        viewModel.showTermsAndConditions.observe(viewLifecycleOwner, EventObserver { show ->
            if (!show) return@EventObserver
            Toast.makeText(context, getString(R.string.not_yet_ready), Toast.LENGTH_SHORT).show()
        })

        viewModel.navigateBack.observe(viewLifecycleOwner, EventObserver { navigateBack ->
            if (!navigateBack) return@EventObserver
            context?.hideSoftKeyboard(binding.root)
            findNavController().popBackStack()
        })
    }
}