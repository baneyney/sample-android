package com.hadjirulricky.gmail.notipig.util.ui

import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.util.FieldValidator

@BindingAdapter("goneUnless")
fun goneUnless(view: View, condition: Boolean) {
    if (condition) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("errorText")
fun setErrorText(textInputLayout: TextInputLayout, text: String) {
    textInputLayout.error = text
}

@BindingAdapter(value = ["passwordText", "showErrorHint"], requireAll = true)
fun passwordErrorHint(
    textInputLayout: TextInputLayout,
    password: String,
    showErrors: Boolean
) {
    if (!showErrors) {
        textInputLayout.error = ""
        return
    }
    val context = textInputLayout.context
    val fieldValidator = FieldValidator()
    val errorMessage = if (password.isEmpty()) {
        context.getString(R.string.field_error_field_is_required)
    } else if (password.length < 8) {
        context.getString(R.string.field_error_password_must_contain_at_least_8_characters)
    } else if (fieldValidator.hasSpaces(password)) {
        context.getString(R.string.field_error_password_should_not_contain_spaces)
    } else if (!fieldValidator.hasNumbers(password)) {
        context.getString(R.string.field_error_password_must_contain_a_number)
    } else if (!fieldValidator.hasSpecialCharacters(password)) {
        context.getString(R.string.field_error_password_must_contain_a_special_character)
    } else {
        ""
    }

    textInputLayout.error = errorMessage
    textInputLayout.invalidate()
}