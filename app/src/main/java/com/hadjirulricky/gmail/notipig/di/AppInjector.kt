package com.hadjirulricky.gmail.notipig.di

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.hadjirulricky.gmail.notipig.NotiPigApplication
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

object AppInjector {
    fun init(notiPigApplication: NotiPigApplication) {
        DaggerAppComponent
            .factory()
            .create(notiPigApplication)
            .inject(notiPigApplication)

        notiPigApplication.registerActivityLifecycleCallbacks(
            object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(
                    activity: Activity,
                    savedInstanceState: Bundle?
                ) {
                    if (activity is Injectable || activity is HasSupportFragmentInjector) {
                        AndroidInjection.inject(activity)
                    }
                    if (activity is FragmentActivity) {
                        activity.supportFragmentManager
                            .registerFragmentLifecycleCallbacks(
                                object : FragmentManager.FragmentLifecycleCallbacks() {
                                    override fun onFragmentPreAttached(
                                        fragmentManager: FragmentManager,
                                        fragment: Fragment,
                                        context: Context
                                    ) {
                                        if (fragment is Injectable) {
                                            AndroidSupportInjection.inject(fragment)
                                        }
                                    }
                                }, true
                            )
                    }
                }

                override fun onActivityStarted(activity: Activity) {
                }

                override fun onActivityResumed(activity: Activity) {
                }

                override fun onActivityPaused(activity: Activity) {
                }

                override fun onActivityStopped(activity: Activity) {
                }

                override fun onActivitySaveInstanceState(
                    activity: Activity,
                    outState: Bundle?
                ) {
                }

                override fun onActivityDestroyed(activity: Activity) {
                }
            }
        )
    }
}