package com.hadjirulricky.gmail.notipig.di.module

import com.hadjirulricky.gmail.notipig.ui.onboarding.signin.SignInFragment
import com.hadjirulricky.gmail.notipig.ui.onboarding.signup.SignUpFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun bindSignUpFragment(): SignUpFragment

    @ContributesAndroidInjector
    abstract fun bindSignInFragment(): SignInFragment
}