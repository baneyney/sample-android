package com.hadjirulricky.gmail.notipig.di.module

import android.content.Context
import com.hadjirulricky.gmail.notipig.NotiPigApplication
import com.hadjirulricky.gmail.notipig.data.repository.IUserRepository
import com.hadjirulricky.gmail.notipig.data.repository.UserRepository
import com.hadjirulricky.gmail.notipig.firebase.FirebaseManager
import com.hadjirulricky.gmail.notipig.util.manager.ISharedPreferencesManager
import com.hadjirulricky.gmail.notipig.util.manager.SharedPreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideContext(application: NotiPigApplication): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideSharedPreferencesManager(context: Context): ISharedPreferencesManager {
        return SharedPreferencesManager(context)
    }

    @Provides
    @Singleton
    fun providesUserRepository(): IUserRepository {
        val firebaseManager = FirebaseManager()
        return UserRepository(firebaseManager)
    }
}