package com.hadjirulricky.gmail.notipig.annotation

/**
 * This annotation is used to make
 * private class open for testing purposes.
 *
 */
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class TestOpen