package com.hadjirulricky.gmail.notipig.ui.onboarding

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.ActivityOnboardingBinding
import com.hadjirulricky.gmail.notipig.di.Injectable
import com.hadjirulricky.gmail.notipig.ui.BaseActivity
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver
import kotlinx.android.synthetic.main.activity_onboarding.nav_host_fragment

class OnBoardingActivity : BaseActivity(), Injectable {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityOnboardingBinding>(
            this,
            R.layout.activity_onboarding
        )
        val viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(OnBoardingActivityViewModel::class.java)

        viewModel.startDestination.observe(this, EventObserver { startDestination ->
            val navHostFragment = nav_host_fragment as NavHostFragment
            val navController = navHostFragment.navController
            val navInflater = navController.navInflater
            val navGraph = navInflater.inflate(R.navigation.onboarding_nav_graph)
            navGraph.startDestination = startDestination
            navHostFragment.navController.graph = navGraph
        })
        viewModel.determineStartDestination()
    }
}
