package com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.hadjirulricky.gmail.notipig.databinding.ItemWalkthroughBinding

class WalkthroughPagerAdapter(
    private val walkthroughItems: List<WalkthroughItem>
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val context = container.context
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = ItemWalkthroughBinding.inflate(
            inflater,
            container,
            false
        )
        binding.image = walkthroughItems[position].image
        binding.message = walkthroughItems[position].message
        container.addView(binding.root)
        return binding.root
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount() = walkthroughItems.size
}