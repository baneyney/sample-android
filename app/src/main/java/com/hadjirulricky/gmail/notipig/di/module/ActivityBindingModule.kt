package com.hadjirulricky.gmail.notipig.di.module

import com.hadjirulricky.gmail.notipig.ui.launch.LaunchActivity
import com.hadjirulricky.gmail.notipig.ui.main.MainActivity
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindLaunchActivity(): LaunchActivity

    @ContributesAndroidInjector
    abstract fun bindOnboardingActivity(): OnBoardingActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}