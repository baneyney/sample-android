package com.hadjirulricky.gmail.notipig.data.repository

import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.firebase.FirebaseManager
import com.hadjirulricky.gmail.notipig.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(
    private val firebaseManager: FirebaseManager
) : IUserRepository {

    override suspend fun signUp(
        email: String,
        password: String,
        firstName: String,
        lastName: String
    ): Resource<User> {
        return withContext(Dispatchers.IO) {
            firebaseManager.signUp(
                email,
                password,
                firstName,
                lastName
            )
        }
    }

    override suspend fun signIn(
        email: String,
        password: String
    ): Resource<User> {
        return withContext(Dispatchers.IO) {
            firebaseManager.signIn(email, password)
        }
    }
}

interface IUserRepository {
    suspend fun signUp(
        email: String,
        password: String,
        firstName: String,
        lastName: String
    ): Resource<User>

    suspend fun signIn(email: String, password: String): Resource<User>
}