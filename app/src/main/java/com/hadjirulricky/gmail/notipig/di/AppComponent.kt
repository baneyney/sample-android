package com.hadjirulricky.gmail.notipig.di

import com.hadjirulricky.gmail.notipig.NotiPigApplication
import com.hadjirulricky.gmail.notipig.di.module.ActivityBindingModule
import com.hadjirulricky.gmail.notipig.di.module.AppModule
import com.hadjirulricky.gmail.notipig.di.module.FragmentBindingModule
import com.hadjirulricky.gmail.notipig.di.module.ViewModelBindingModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class,
        ViewModelBindingModule::class
    ]
)

interface AppComponent : AndroidInjector<NotiPigApplication> {

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<NotiPigApplication>
}