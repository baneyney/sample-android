package com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.FragmentWalkthroughBinding
import com.hadjirulricky.gmail.notipig.util.extension.findNavController
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver

class WalkthroughFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentWalkthroughBinding>(
            inflater,
            R.layout.fragment_walkthrough,
            null,
            false
        )
        val viewModel = ViewModelProviders.of(this).get(WalkthroughFragmentViewModel::class.java)
        binding.viewModel = viewModel
        binding.viewpagerWalkthroughList.adapter = WalkthroughPagerAdapter(
            generateWalkthroughItemList()
        )
        binding.tablayoutPageIndicator.setupWithViewPager(binding.viewpagerWalkthroughList, true)

        viewModel.navigateToNextScreen.observe(
            viewLifecycleOwner,
            EventObserver { navigateToNextScreen ->
                if (!navigateToNextScreen) return@EventObserver
                findNavController().navigate(R.id.action_walkthroughFragment_to_signInFragment)
            }
        )

        return binding.root
    }

    private fun generateWalkthroughItemList(): List<WalkthroughItem> {
        return arrayListOf(
            WalkthroughItem(
                android.R.color.white,
                getString(R.string.walkthrough_description)
            ),
            WalkthroughItem(
                android.R.color.white,
                getString(R.string.walkthrough_description)
            ),
            WalkthroughItem(
                android.R.color.white,
                getString(R.string.walkthrough_description)
            )
        )
    }
}
