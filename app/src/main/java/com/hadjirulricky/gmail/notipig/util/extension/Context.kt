package com.hadjirulricky.gmail.notipig.util.extension

import android.content.Context
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog

fun Context.hideSoftKeyboard(view: View) {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun Context?.showAlertDialog(
    title: String,
    message: String?,
    positiveButtonText: String = "",
    positiveButtonFunction: () -> Unit = {},
    negativeButtonText: String = "",
    negativeButtonFunction: () -> Unit = {}
) {
    if (this == null) return
    val alertDialog = AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message ?: "")
        .setCancelable(false)
        .create()

    if (positiveButtonText.isNotEmpty()) {
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveButtonText) { dialog, _ ->
            dialog.dismiss()
            positiveButtonFunction.invoke()
        }
    }

    if (negativeButtonText.isNotEmpty()) {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButtonText) { dialog, _ ->
            dialog.dismiss()
            negativeButtonFunction.invoke()
        }
    }
    alertDialog.show()
}