package com.hadjirulricky.gmail.notipig.ui.launch

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.databinding.ActivityLaunchBinding
import com.hadjirulricky.gmail.notipig.di.Injectable
import com.hadjirulricky.gmail.notipig.ui.BaseActivity
import com.hadjirulricky.gmail.notipig.ui.main.MainActivity
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivity
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver

class LaunchActivity : BaseActivity(), Injectable {
    private lateinit var binding: ActivityLaunchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_launch)
        val viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(LaunchActivityViewModel::class.java)

        viewModel.launchDestination.observe(this, EventObserver { launchDestination ->
            val intent = Intent(
                this@LaunchActivity,
                when (launchDestination) {
                    LaunchDestination.ONBOARDING -> {
                        OnBoardingActivity::class.java
                    }
                    else -> {
                        MainActivity::class.java
                    }
                }
            )
            startActivity(intent)
            finish()
        })
        viewModel.startLaunchCountdown()
    }
}