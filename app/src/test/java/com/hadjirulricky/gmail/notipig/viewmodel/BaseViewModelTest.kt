package com.hadjirulricky.gmail.notipig.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hadjirulricky.gmail.notipig.util.CoroutinesTestRule
import org.junit.Rule
import org.junit.rules.TestRule

abstract class BaseViewModelTest {

    @JvmField
    @Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()
}