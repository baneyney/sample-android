package com.hadjirulricky.gmail.notipig.viewmodel.onboarding

import com.hadjirulricky.gmail.notipig.TestConstants
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.mock.repository.MockUserRepository
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.ui.onboarding.signup.SignUpFragmentViewModel
import com.hadjirulricky.gmail.notipig.util.livedata.EventObserver
import com.hadjirulricky.gmail.notipig.viewmodel.BaseViewModelTest
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SignUpFragmentViewModelTest : BaseViewModelTest() {
    private lateinit var viewModel: SignUpFragmentViewModel
    private lateinit var mockUserRepository: MockUserRepository

    @Before
    fun before() {
        mockUserRepository = MockUserRepository()
        viewModel = SignUpFragmentViewModel(mockUserRepository)
    }

    @Test
    fun onTermsAndConditionsClicked() {
        viewModel.onTermsAndConditionsClicked()
        Assert.assertEquals(
            true,
            viewModel.showTermsAndConditions.value?.getContentIfNotHandled()
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun signUpSuccessful() = coroutinesTestRule.testDispatcher.runBlockingTest {
        viewModel.firstName.value = TestConstants.RegisteredAccount.FIRSTNAME
        viewModel.lastName.value = TestConstants.RegisteredAccount.LASTNAME
        viewModel.password.value = TestConstants.RegisteredAccount.PASSWORD
        viewModel.email.value = TestConstants.UnregisteredAccount.EMAIL

        val signUpNetworkResponseObserver = mock<EventObserver<Resource<User>>>()
        viewModel.signUpNetworkResponse.observeForever(signUpNetworkResponseObserver)

        viewModel.onCreateAccountButtonClicked()

        Assert.assertEquals(
            TestConstants.MockUser,
            viewModel.signUpNetworkResponse.value?.getContentIfNotHandled()?.data
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun signUpFailed() = coroutinesTestRule.testDispatcher.runBlockingTest {
        viewModel.firstName.value = TestConstants.RegisteredAccount.FIRSTNAME
        viewModel.lastName.value = TestConstants.RegisteredAccount.LASTNAME
        viewModel.password.value = TestConstants.RegisteredAccount.PASSWORD
        viewModel.email.value = TestConstants.RegisteredAccount.EMAIL

        val signUpNetworkResponseObserver = mock<EventObserver<Resource<User>>>()
        viewModel.signUpNetworkResponse.observeForever(signUpNetworkResponseObserver)

        viewModel.onCreateAccountButtonClicked()

        Assert.assertEquals(
            "The email address is already in use by another account.",
            viewModel.signUpNetworkResponse.value?.getContentIfNotHandled()?.message
        )
    }

    @Test
    fun shouldFailValidation() {
        viewModel.firstName.value = ""
        viewModel.lastName.value = ""
        viewModel.password.value = TestConstants.Validation.INVALID_PASSWORD
        viewModel.email.value = TestConstants.Validation.INVALID_EMAIL

        viewModel.onCreateAccountButtonClicked()
        Assert.assertEquals(
            true,
            viewModel.showErrors.value
        )
    }
}