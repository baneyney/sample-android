package com.hadjirulricky.gmail.notipig.viewmodel.onboarding

import androidx.lifecycle.Observer
import com.hadjirulricky.gmail.notipig.TestConstants
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.mock.repository.MockUserRepository
import com.hadjirulricky.gmail.notipig.ui.onboarding.signin.SignInFragmentViewModel
import com.hadjirulricky.gmail.notipig.util.Resource
import com.hadjirulricky.gmail.notipig.util.livedata.Event
import com.hadjirulricky.gmail.notipig.viewmodel.BaseViewModelTest
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SignInFragmentViewModelTest : BaseViewModelTest() {
    private lateinit var viewModel: SignInFragmentViewModel

    @Before
    fun before() {
        viewModel = SignInFragmentViewModel(MockUserRepository())
    }

    @Test
    fun onCreateAccountClicked() {
        viewModel.onCreateAccountClicked()
        Assert.assertEquals(
            true,
            viewModel.navigateToSignUp.value?.getContentIfNotHandled()
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun signInSuccessful() = coroutinesTestRule.testDispatcher.runBlockingTest {
        viewModel.email.value = TestConstants.RegisteredAccount.EMAIL
        viewModel.password.value = TestConstants.RegisteredAccount.PASSWORD

        val signInNetworkResponseObserver = mock<Observer<Event<Resource<User>>>>()
        viewModel.signInNetworkResponse.observeForever(signInNetworkResponseObserver)

        viewModel.onSignInButtonClicked()

        val expectedUser = TestConstants.MockUser

        //val expectedUser = User("", "a", "a", "d")

        Assert.assertEquals(
            expectedUser.email,
            viewModel.signInNetworkResponse.value?.getContentIfNotHandled()?.data?.email
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun signInFailed() = coroutinesTestRule.testDispatcher.runBlockingTest {
        viewModel.email.value = TestConstants.UnregisteredAccount.EMAIL
        viewModel.password.value = TestConstants.UnregisteredAccount.PASSWORD

        val signInNetworkResponseObserver = mock<Observer<Event<Resource<User>>>>()
        viewModel.signInNetworkResponse.observeForever(signInNetworkResponseObserver)

        viewModel.onSignInButtonClicked()

        Assert.assertEquals(
            "There is no user record corresponding to this identifier. The user may have been deleted.",
            viewModel.signInNetworkResponse.value?.getContentIfNotHandled()?.message
        )
    }

    @Test
    fun shouldFailValidation() {
        viewModel.email.value = TestConstants.Validation.INVALID_EMAIL
        viewModel.password.value = TestConstants.Validation.INVALID_PASSWORD
        viewModel.onSignInButtonClicked()
        Assert.assertEquals(
            true,
            viewModel.showErrors.value
        )
    }
}