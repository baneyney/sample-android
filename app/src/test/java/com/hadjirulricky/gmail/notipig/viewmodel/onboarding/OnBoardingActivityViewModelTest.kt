package com.hadjirulricky.gmail.notipig.viewmodel.onboarding

import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivityViewModel
import com.hadjirulricky.gmail.notipig.util.Constants.SharedPreferencesKey.FIRST_INSTALL
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import com.hadjirulricky.gmail.notipig.viewmodel.BaseViewModelTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class OnBoardingActivityViewModelTest : BaseViewModelTest() {
    private lateinit var viewModel: OnBoardingActivityViewModel
    private lateinit var mockSharedPreferencesManager: MockSharedPreferencesManager

    @Before
    fun before() {
        mockSharedPreferencesManager =
            MockSharedPreferencesManager()
        viewModel = OnBoardingActivityViewModel(mockSharedPreferencesManager)
    }

    @Test
    fun shouldNavigateToWalkThroughIfFirstInstall() {
        mockSharedPreferencesManager.put(FIRST_INSTALL, true)
        viewModel.determineStartDestination()
        Assert.assertEquals(
            R.id.walkthroughFragment,
            viewModel.startDestination.value?.getContentIfNotHandled()
        )
    }

    @Test
    fun shouldNavigateToAuthenticationIfNotFirstInstall() {
        mockSharedPreferencesManager.put(FIRST_INSTALL, false)
        viewModel.determineStartDestination()
        Assert.assertEquals(
            R.id.signInFragment,
            viewModel.startDestination.value?.getContentIfNotHandled()
        )
    }
}