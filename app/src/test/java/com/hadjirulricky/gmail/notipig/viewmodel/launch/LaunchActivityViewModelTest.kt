package com.hadjirulricky.gmail.notipig.viewmodel.launch

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.hadjirulricky.gmail.notipig.ui.launch.LaunchActivityViewModel
import com.hadjirulricky.gmail.notipig.ui.launch.LaunchDestination
import com.hadjirulricky.gmail.notipig.util.Constants
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LaunchActivityViewModelTest {

    @JvmField
    @Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: LaunchActivityViewModel
    private lateinit var mockSharedPreferencesManager: MockSharedPreferencesManager

    @Before
    fun before() {
        mockSharedPreferencesManager =
            MockSharedPreferencesManager()
        viewModel = LaunchActivityViewModel(mockSharedPreferencesManager)
    }

    @Test
    fun shouldNavigateToOnBoardingScreen() {
        mockSharedPreferencesManager.put(Constants.SharedPreferencesKey.LOGIN_COMPLETED, false)
        viewModel.onLaunchScreenCountDownEnded()
        Assert.assertEquals(
            LaunchDestination.ONBOARDING,
            viewModel.launchDestination.value?.getContentIfNotHandled()
        )
    }

    @Test
    fun shouldNavigateToMainActivityScreen() {
        mockSharedPreferencesManager.put(Constants.SharedPreferencesKey.LOGIN_COMPLETED, true)
        viewModel.onLaunchScreenCountDownEnded()
        Assert.assertEquals(
            LaunchDestination.MAIN_ACTIVITY,
            viewModel.launchDestination.value?.getContentIfNotHandled()
        )
    }
}