package com.hadjirulricky.gmail.notipig.viewmodel.onboarding

import com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough.WalkthroughFragmentViewModel
import com.hadjirulricky.gmail.notipig.viewmodel.BaseViewModelTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class WalkthroughFragmentViewModelTest : BaseViewModelTest() {
    private lateinit var viewModel: WalkthroughFragmentViewModel

    @Before
    fun before() {
        viewModel = WalkthroughFragmentViewModel()
    }

    @Test
    fun onStartButtonClicked() {
        viewModel.onStartButtonClicked()
        Assert.assertEquals(
            true,
            viewModel.navigateToNextScreen.value?.getContentIfNotHandled()
        )
    }
}