package com.hadjirulricky.gmail.notipig.mock.manager

import com.hadjirulricky.gmail.notipig.util.manager.ISharedPreferencesManager

class MockSharedPreferencesManager : ISharedPreferencesManager {
    companion object {
        val values = mutableMapOf<String, Any>()
    }

    override fun getString(key: String, defaultValue: String): String {
        return values[key] as? String ?: defaultValue
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        return values[key] as? Int ?: defaultValue
    }

    override fun getLong(key: String, defaultValue: Long): Long {
        return values[key] as? Long ?: defaultValue
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return values[key] as? Boolean ?: defaultValue
    }

    override fun <T> put(key: String, value: T) {
        values[key] = value as Any
    }
}