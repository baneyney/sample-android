package com.hadjirulricky.gmail.notipig.mock.repository

import com.hadjirulricky.gmail.notipig.TestConstants
import com.hadjirulricky.gmail.notipig.data.model.User
import com.hadjirulricky.gmail.notipig.data.repository.IUserRepository
import com.hadjirulricky.gmail.notipig.util.Resource

class MockUserRepository : IUserRepository {

    override suspend fun signUp(
        email: String,
        password: String,
        firstName: String,
        lastName: String
    ): Resource<User> {
        val result: Resource<User>
        if (email != TestConstants.RegisteredAccount.EMAIL) {
            result = Resource.Success(TestConstants.MockUser)
        } else {
            result = Resource.Error(
                "The email address is already in use by another account."
            )
        }
        return result
    }

    override suspend fun signIn(email: String, password: String): Resource<User> {
        val result: Resource<User>
        if (email == TestConstants.RegisteredAccount.EMAIL &&
            password == TestConstants.RegisteredAccount.PASSWORD) {
            result = Resource.Success(TestConstants.MockUser)
        } else {
            result = Resource.Error(
                "There is no user record corresponding to this identifier. The user may have been deleted."
            )
        }
        return result
    }
}