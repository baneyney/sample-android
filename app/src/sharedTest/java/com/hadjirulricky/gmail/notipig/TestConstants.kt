package com.hadjirulricky.gmail.notipig

import com.hadjirulricky.gmail.notipig.TestConstants.RegisteredAccount.EMAIL
import com.hadjirulricky.gmail.notipig.TestConstants.RegisteredAccount.FIRSTNAME
import com.hadjirulricky.gmail.notipig.TestConstants.RegisteredAccount.LASTNAME
import com.hadjirulricky.gmail.notipig.TestConstants.RegisteredAccount.UID
import com.hadjirulricky.gmail.notipig.data.model.User

object TestConstants {

    object RegisteredAccount {
        const val EMAIL = "amirahcatungal@gmail.com"
        const val PASSWORD = "Qwerty@1"
        const val UID = "SsfsfAFOIPOIsf"
        const val FIRSTNAME = "Amirah"
        const val LASTNAME = "Catungal"
    }

    object UnregisteredAccount {
        const val EMAIL = "hadjirulricky@gmail.com"
        const val PASSWORD = "Qwerty@1"
    }

    object Validation {
        const val INVALID_EMAIL = "invalid_email"
        const val INVALID_PASSWORD = ""
    }

    object MockUser : User(
        UID,
        EMAIL,
        FIRSTNAME,
        LASTNAME
    )
}