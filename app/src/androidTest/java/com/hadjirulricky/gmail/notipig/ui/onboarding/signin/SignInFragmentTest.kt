package com.hadjirulricky.gmail.notipig.ui.onboarding.signin

import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.TestConstants
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivity
import com.hadjirulricky.gmail.notipig.util.Constants
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper.Assertion.viewContainsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SignInFragmentTest {

    @get:Rule
    val activityRule: ActivityTestRule<OnBoardingActivity> =
        object : ActivityTestRule<OnBoardingActivity>(OnBoardingActivity::class.java) {
            override fun getActivityIntent(): Intent {
                MockSharedPreferencesManager
                    .values[Constants.SharedPreferencesKey.FIRST_INSTALL] = false

                val targetContext = InstrumentationRegistry.getInstrumentation()
                    .targetContext
                return Intent(targetContext, OnBoardingActivity::class.java)
            }
        }

    @MediumTest
    @Test
    fun testUIElements() {
        Espresso.closeSoftKeyboard()
        val viewIds = intArrayOf(
            R.id.imageview_logo,
            R.id.textinputlayout_email,
            R.id.textinputedittext_email,
            R.id.textinputlayout_password,
            R.id.textinputedittext_password,
            R.id.button_signin,
            R.id.textview_create_account
        )
        // Test visibility
        TestHelper.Assertion.assertViewsAreDisplayed(*viewIds)

        // Test UI events
        // Invalid email and password
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_email,
            TestConstants.Validation.INVALID_EMAIL
        )
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_password,
            TestConstants.Validation.INVALID_PASSWORD
        )

        TestHelper.Event.performClick(R.id.button_signin)

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_email,
            "Please input a valid email."
        )

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_password,
            "Please input your password."
        )

        // Test UI events
        signInFailedTest()
        signInSuccessTest()
    }

    private fun signInFailedTest() {
        TestHelper.Event.performClearTextViews(
            intArrayOf(R.id.textinputedittext_email, R.id.textinputedittext_password)
        )
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_email,
            TestConstants.UnregisteredAccount.EMAIL
        )
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_password,
            TestConstants.UnregisteredAccount.PASSWORD
        )

        TestHelper.Event.performClick(R.id.button_signin)

        viewContainsString(
            android.R.id.message,
            "There is no user record corresponding to this identifier. The user may have been deleted."
        )
        onView(withId(android.R.id.button1))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    private fun signInSuccessTest() {
        TestHelper.Event.performClearTextViews(
            intArrayOf(R.id.textinputedittext_email, R.id.textinputedittext_password)
        )
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_email,
            TestConstants.RegisteredAccount.EMAIL
        )
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_password,
            TestConstants.RegisteredAccount.PASSWORD
        )
        TestHelper.Event.performClick(R.id.button_signin)

        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_main, visible = true)
    }
}