package com.hadjirulricky.gmail.notipig.ui.launch

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import com.hadjirulricky.gmail.notipig.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LaunchActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(LaunchActivity::class.java)

    @MediumTest
    @Test
    fun testUIElements() {
        onView(withId(R.id.layout_launch)).check(matches(isDisplayed()))
        onView(withId(R.id.imageview_logo)).check(matches(isDisplayed()))
    }
}