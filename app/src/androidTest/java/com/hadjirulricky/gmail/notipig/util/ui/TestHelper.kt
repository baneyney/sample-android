package com.hadjirulricky.gmail.notipig.util.ui

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.actionWithAssertions
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.google.android.material.textfield.TextInputLayout
import com.hadjirulricky.gmail.notipig.util.ui.CustomMatchers.withErrorText
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.hamcrest.TypeSafeMatcher

object TestHelper {

    object Assertion {
        fun assertViewIsDisplayed(viewId: Int, visible: Boolean): Boolean {
            val matches = if (visible) isDisplayed() else not(isDisplayed())
            return try {
                onView(allOf(withId(viewId))).check(matches(matches))
                true
            } catch (e: NoMatchingViewException) {
                false
            }
        }

        fun assertViewsAreDisplayed(
            vararg viewIds: Int,
            inScrollableView: Boolean = false
        ): List<ViewInteraction> {
            return when (inScrollableView) {
                true -> viewIds.map {
                    onView(
                        allOf(withId(it), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
                    ).perform(actionWithAssertions(CustomScrollToAction()))
                        .check(matches(isDisplayed()))
                }
                false -> viewIds.map {
                    onView(
                        allOf(withId(it), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
                    ).check(matches(isDisplayed()))
                }
            }
        }

        fun assertTextInputLayoutErrorTextIsDisplayed(
            id: Int,
            expectedError: String
        ): ViewInteraction {
            return onView(withId(id)).check(matches(withErrorText(expectedError)))
        }

        fun viewContainsString(textViewId: Int, expectedString: String) {
            onView(withId(textViewId)).check(matches(withText(expectedString)))
        }
    }

    object Event {
        fun performClick(viewId: Int, inScrollableView: Boolean = false): ViewInteraction {
            return when (inScrollableView) {
                true -> {
                    onView(
                        allOf(
                            withId(viewId), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                        )
                    ).perform(scrollTo(), click())
                }
                false -> {
                    onView(
                        allOf(
                            withId(viewId), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                        )
                    ).perform(click())
                }
            }
        }

        fun performClearTextViews(editTextIds: IntArray): List<ViewInteraction> {
            for (editTextId in editTextIds) {
                onView(withId(editTextId)).perform(clearText())
            }

            return editTextIds.map { onView(withId(it)).check(matches(isDisplayed())) }
        }

        fun performTypeText(viewId: Int, text: String): ViewInteraction {
            return onView(
                allOf(withId(viewId), withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
            ).perform(typeText(text), closeSoftKeyboard())
        }
    }
}

object CustomMatchers {
    fun withErrorText(expectedErrorText: String): Matcher<View> {
        return object : TypeSafeMatcher<View>() {

            override fun matchesSafely(view: View): Boolean {
                if (view !is TextInputLayout) {
                    return false
                }

                val error = (view).error ?: return false

                val hint = error.toString()

                return expectedErrorText == hint
            }

            override fun describeTo(description: Description) {
                description.appendText("with error  content: $expectedErrorText")
            }
        }
    }
}