package com.hadjirulricky.gmail.notipig.ui.onboarding.walkthrough

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivity
import com.hadjirulricky.gmail.notipig.util.Constants
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WalkthroughFragmentTest {

    @get:Rule
    val activityRule: ActivityTestRule<OnBoardingActivity> =
        object : ActivityTestRule<OnBoardingActivity>(OnBoardingActivity::class.java) {
            override fun getActivityIntent(): Intent {
                MockSharedPreferencesManager
                    .values[Constants.SharedPreferencesKey.FIRST_INSTALL] = true

                val targetContext = InstrumentationRegistry.getInstrumentation()
                    .targetContext
                return Intent(targetContext, OnBoardingActivity::class.java)
            }
        }

    @MediumTest
    @Test
    fun testUIElements() {
        val viewIds = intArrayOf(
            R.id.viewpager_walkthrough_list,
            R.id.tablayout_page_indicator,
            R.id.button_start
        )

        // Test UI visibility
        TestHelper.Assertion.assertViewsAreDisplayed(*viewIds)

        // Test UI events
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeLeft())
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeLeft())
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeLeft())
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeRight())
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeRight())
        onView(withId(R.id.viewpager_walkthrough_list)).perform(swipeRight())

        onView(withId(R.id.button_start)).perform(click())
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_signin, true)
    }
}
