package com.hadjirulricky.gmail.notipig.ui.onboarding.signup

import android.content.Intent
import androidx.navigation.fragment.NavHostFragment
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.TestConstants
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import com.hadjirulricky.gmail.notipig.ui.onboarding.OnBoardingActivity
import com.hadjirulricky.gmail.notipig.util.Constants
import com.hadjirulricky.gmail.notipig.util.extension.findNavController
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper.Assertion.viewContainsString
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SignUpFragmentTest {

    @get:Rule
    val activityRule: ActivityTestRule<OnBoardingActivity> =
        object : ActivityTestRule<OnBoardingActivity>(OnBoardingActivity::class.java) {
            override fun getActivityIntent(): Intent {
                MockSharedPreferencesManager
                    .values[Constants.SharedPreferencesKey.FIRST_INSTALL] = false

                val targetContext = InstrumentationRegistry.getInstrumentation()
                    .targetContext
                return Intent(targetContext, OnBoardingActivity::class.java)
            }
        }

    @Before
    fun before() {
        val navHostFragment =
            activityRule.activity.supportFragmentManager.fragments.first() as NavHostFragment
        val currentFragment = navHostFragment.childFragmentManager.fragments.last()
        currentFragment.findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
    }

    @MediumTest
    @Test
    fun testUIElements() {
        Espresso.closeSoftKeyboard()
        val viewIds = intArrayOf(
            R.id.icon_back,
            R.id.textview_header,
            R.id.textinputedittext_email,
            R.id.textinputlayout_firstname,
            R.id.textinputedittext_firstname,
            R.id.textinputlayout_lastname,
            R.id.textinputedittext_lastname,
            R.id.textinputlayout_email,
            R.id.textinputedittext_email,
            R.id.textinputlayout_password,
            R.id.textinputedittext_password,
            R.id.button_signup,
            R.id.textview_terms_and_conditions_header,
            R.id.textview_terms_and_conditions
        )
        // Test visibility
        TestHelper.Assertion.assertViewsAreDisplayed(*viewIds)

        // Test Ui events
        showErrors()
        inputRegisteredAccount()
        registerNewAccount()
    }

    private fun showErrors() {
        TestHelper.Event.performClick(R.id.button_signup)

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_firstname,
            "Field is required."
        )

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_lastname,
            "Field is required."
        )

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_email,
            "Please input a valid email."
        )

        TestHelper.Assertion.assertTextInputLayoutErrorTextIsDisplayed(
            R.id.textinputlayout_password,
            "Field is required."
        )
    }

    private fun inputRegisteredAccount() {
        TestHelper.Event.performTypeText(
            R.id.textinputedittext_firstname,
            TestConstants.RegisteredAccount.FIRSTNAME
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_lastname,
            TestConstants.RegisteredAccount.LASTNAME
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_email,
            TestConstants.RegisteredAccount.EMAIL
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_password,
            TestConstants.UnregisteredAccount.PASSWORD
        )

        TestHelper.Event.performClick(R.id.button_signup)

        viewContainsString(
            android.R.id.message,
            "The email address is already in use by another account."
        )
        onView(withId(android.R.id.button1))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    private fun registerNewAccount() {
        val editTextIds = intArrayOf(
            R.id.textinputedittext_firstname,
            R.id.textinputedittext_lastname,
            R.id.textinputedittext_email,
            R.id.textinputedittext_password
        )
        TestHelper.Event.performClearTextViews(editTextIds)

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_firstname,
            TestConstants.RegisteredAccount.FIRSTNAME
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_lastname,
            TestConstants.RegisteredAccount.LASTNAME
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_email,
            TestConstants.UnregisteredAccount.EMAIL
        )

        TestHelper.Event.performTypeText(
            R.id.textinputedittext_password,
            TestConstants.UnregisteredAccount.PASSWORD
        )

        TestHelper.Event.performClick(R.id.button_signup)

        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_main, visible = true)
    }
}