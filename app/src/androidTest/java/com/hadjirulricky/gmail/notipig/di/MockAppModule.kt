package com.hadjirulricky.gmail.notipig.di

import com.hadjirulricky.gmail.notipig.data.repository.IUserRepository
import com.hadjirulricky.gmail.notipig.mock.manager.MockSharedPreferencesManager
import com.hadjirulricky.gmail.notipig.mock.repository.MockUserRepository
import com.hadjirulricky.gmail.notipig.util.manager.ISharedPreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MockAppModule {

    @Provides
    @Singleton
    fun provideSharedPreferencesManager(): ISharedPreferencesManager {
        return MockSharedPreferencesManager()
    }

    @Provides
    @Singleton
    fun providesUserRepository(): IUserRepository {
        return MockUserRepository()
    }
}