package com.hadjirulricky.gmail.notipig.ui.main

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import com.hadjirulricky.gmail.notipig.R
import com.hadjirulricky.gmail.notipig.util.ui.TestHelper
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @MediumTest
    @Test
    fun testUIElements() {
        // Test starting destination
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_today_task, visible = true)

        // Test navigation
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.missedTaskFragment))
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_missed_task, visible = true)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.allTaskFragment))
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_all_task, visible = true)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.manageTasksFragment))
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_manage_tasks, visible = true)

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.settingsFragment))
        TestHelper.Assertion.assertViewIsDisplayed(R.id.layout_settings, visible = true)
    }
}