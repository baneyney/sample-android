package com.hadjirulricky.gmail.notipig.di

import com.hadjirulricky.gmail.notipig.TestApplication
import com.hadjirulricky.gmail.notipig.di.module.ActivityBindingModule
import com.hadjirulricky.gmail.notipig.di.module.FragmentBindingModule
import com.hadjirulricky.gmail.notipig.di.module.ViewModelBindingModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        MockAppModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class,
        ViewModelBindingModule::class
    ]
)
interface MockAppComponent : AndroidInjector<TestApplication> {
    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<TestApplication>
}